use std::io;

// Brings the input/output library into scope,
// which comes from the standard library "std".

use rand::Rng;

// Brings the random number generator into scope.

use std::cmp::Ordering;

// Brings the ordering enum into scope.

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1,101);

    println!("The secret number is: {}:", secret_number);
    loop {
        println!("Please input your guess of the secret number...");

        let mut guess = String::new();

        io::stdin() // sets a mutable variable "guess" from std io.
            .read_line(&mut guess) // places user input into the string "guesss".
            .expect("Failed to read line");

        // Shadows the guess string variable with a new guess variable of type u32.
        // First trims the string to remove whitespaces then parse the string type
        // to type u32. The "parse" method parses a string into a number, in this
        // case an unsigned (zero or positive) integer.
        // The "match" method enforces that the guess var is of type u32,
        // If not, then continues loop.
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        // prints the "guess" variable.
        //   println!("You guessed: {}", guess);
        
        // Compares the guess u32 variable to the secret_number variable.
        // Breaks loop when guess and secret_number variables are equal.
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
                
        }
    }
}

