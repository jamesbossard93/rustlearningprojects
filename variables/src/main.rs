fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);

    x = 6; // Only available if x is mutable.
    println!("The value of x is: {}", x);

    // Shadowing a variable, allows the user to use simple 
    // transformations. Must use the "let" keyword.
    let y = 5;

    let y = y + 1;

    let y = y * 2;

    println!("The value of y is: {}", y);

    test_function(x);

    let condition = true;

    let number = if condition { 6 } else { 0 };

    println!("The number of condition is: {}", number);

}

fn test_function(x: i32) {
    println!("The value of x is: {}", x);
}

