fn main() {
    
    test_while_loop(5);

    countdown(10);
}

fn test_while_loop(stop: i32) {

    let mut x: i32 = 0;

    while x < stop {
        println!("counting X at: {}", x);

        x += 1;
    }
}

fn countdown(start: i32) {

    for number in (1..start).rev() {
        println!("{}!", number);
    }
    println!("LIFT OFF!!!");
}

