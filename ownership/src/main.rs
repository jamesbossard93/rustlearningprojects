fn main() {

    // OWNERSHIP RULES:
    // 
    // 1. Each value has a variable called its owner.
    // 2. There can only be one owner at a time.
    // 3. When the owner goes out of scope, the value will be dropped.
    //


    // Creates an immutable string literal of fixed memory length.
    let s = "hello";
    
    // Creates a mutable string of unknown memory size allocated on the heap.
    // The data length can be changed during runtime and is "dropped" when
    // it goes out of scope ( using {}).
    let mut t = String::from("hello");

    // The pointer, length and capacity of s1 is copied into s2 onto the stack.
    // The data which contains "hello" remains in the heap. Only the "reference"
    // from s1 is copied. Because of this, if s1 goes out of scope, then s2
    // will also go out of scope.
    let s1 = String::from("hello");
    let s2 = s1;

    // Shallow Copy, Deep Copy, AKA Move
    //
    // Rust never automatically makes deep copies, only shallow copies
    // (i.e. pointer, length etc.. are copied).
    //
    // Shallow Copy: let a = b;
    // Deep Copy: let b = a.clone();

    // References and Borrowing
    //
    // &DataType is referencing a variable of type DataType.
    // The opposite of &DataType is *DataType to dereference.
    //
    // In order to change a ref (&x) you need to use &mut because
    // references are immutable by default.
    //
    // THE RULES OF REFERENCSE:
    //  - At any given time, there can only be either one mutable
    //  reference or any given number of immutable references.
    //
    //  - References must always be valid (not null!).


    // THE SLICE TYPE
    println!("Hello, world!");
}
